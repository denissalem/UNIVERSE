# PROOF OF CONCEPT: Improved Perlin Noise

Small application under GNU/GPLv3 implementing `improved Perlin noise`.

The generated `heightmap` is then saved as PNG image.

# Compiling

You will need `libpng` installed and available to your compiler.

```
make
```

# Clean the binary and any `.o` object

    make clean

# How to

Once compiled

    ./improvedPerlinNoise <power of two>


Something like

    ./improvedPerlinNoise 256


or


    ./improvedPerlinNoise 512


A heightmap is then generated and saved as `terrain.png` in the current working directory.
