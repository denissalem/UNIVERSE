﻿#include <stdio.h>
#include <stdlib.h>

#include "improved_perlin_noise.h"
#include "png.h"

int main(int argc, char ** argv) {
    if (argc == 1) {
        printf("usage: ./improvedPerlinNoise <power of two>\n");
        return 0;
    }
    
    unsigned int scale = atoi(argv[1]);
    
    Heightmap heightmap = getHeightmapFromPerlinNoise2D(scale);
        
    double normalizedIntensity;
    PIXEL ** matrix = (PIXEL **) malloc( scale * sizeof(PIXEL * ));
    for(unsigned int j=0;j<scale;j++) {
        matrix[j] = (PIXEL *) malloc( scale * sizeof(PIXEL));
        for (unsigned int i=0; i< scale;i++) {
            normalizedIntensity = (heightmap.buffer[ i + j * scale ] - heightmap.min) * ( 1.0 / (heightmap.max-heightmap.min) );
            matrix[j][i].Alpha  = 255;
            matrix[j][i].Red    = (char) (normalizedIntensity * 255);
            matrix[j][i].Blue   = (char) (normalizedIntensity * 255);
            matrix[j][i].Green  = (char) (normalizedIntensity * 255);
        }
    }
    
    writePng(matrix, scale, "terrain.png");
    return 0;
}
