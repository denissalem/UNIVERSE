#ifndef UNIVERSE_PNG_H
#define UNIVERSE_PNG_H

typedef struct _PIXEL {
     unsigned char Red;      
     unsigned char Green;    
     unsigned char Blue;
     unsigned char Alpha;
} PIXEL;

int writePng( PIXEL ** matrix, int size, const char * filename);

#endif
