#include <stdlib.h>
#include <time.h>

#include "improved_perlin_noise.h"

static int GRADIENTS[8][2] = {
    {1,1},    {-1,1},   {1,-1},   {-1,-1}, {1,0},    {-1,0},   {0,1},    {0,-1},
};

static void initialize_permutation_table(unsigned int P[]) {
    srandom(time(NULL));

    for (int i = 0; i < 256; i++) {
        P[i] = i;
    }
    
    for (int i = 0; i < 255; i++) {
        int offset = (i + random()) % 255;
        int tmp = P[i];
        P[i] = P[offset];
        P[offset] = tmp;
    }
}

static double perlinNoise2D(unsigned int x, unsigned int y, unsigned int scale, unsigned int P[]) {
    double p[2] = {
        (double) x / scale,
        (double) y / scale
    };
    
    unsigned int Q1[2] = {
        (unsigned int) p[0],
        (unsigned int) p[1]
    };
    
    unsigned int Q2[2] = {
        Q1[0] + 1,
        Q1[1]
    };
    
    unsigned int Q3[2] = {
        Q2[0],
        Q1[1] + 1
    };
    
    unsigned int Q4[2] = {
        Q1[0],
        Q3[1]
    };
    
    unsigned int G1 = P[ (Q1[0] + P[ Q1[1] & 255] + scale) & 255 ] & 7;
    unsigned int G2 = P[ (Q2[0] + P[ Q2[1] & 255] + scale) & 255 ] & 7;
    unsigned int G3 = P[ (Q3[0] + P[ Q3[1] & 255] + scale) & 255 ] & 7;
    unsigned int G4 = P[ (Q4[0] + P[ Q4[1] & 255] + scale) & 255 ] & 7;
    
    double s = GRADIENTS[G1][0] * (p[0]-Q1[0]) + GRADIENTS[G1][1] * (p[1]-Q1[1]);
    double t = GRADIENTS[G2][0] * (p[0]-Q2[0]) + GRADIENTS[G2][1] * (p[1]-Q2[1]);
    double u = GRADIENTS[G3][0] * (p[0]-Q3[0]) + GRADIENTS[G3][1] * (p[1]-Q3[1]);
    double v = GRADIENTS[G4][0] * (p[0]-Q4[0]) + GRADIENTS[G4][1] * (p[1]-Q4[1]);
        
    double iX = interpolation((p[0]-Q1[0]));
    
    double iST = s + iX * (t-s);
    double iUV = v + iX * (u-v);
    
    double iY = interpolation((p[1]-Q1[1]));
    
    return (1 + iST + iY * (iUV - iST)) * 0.5;
}

Heightmap getHeightmapFromPerlinNoise2D(unsigned int scale) {
    unsigned int P[256] = {0};
    initialize_permutation_table(P);
    
    Heightmap heightmap = {0};
    heightmap.buffer = (double *) malloc(sizeof(double) * scale * scale);
    heightmap.min = 2;
    heightmap.max = 0;
    
    double coef = 0; 
    
    for(unsigned int j=0; j<scale;j++) {
        for(unsigned int i=0; i<scale;i++) {
            coef = 1.0;
            heightmap.buffer[ i + j * scale] = 0;
            for(unsigned int k=scale/2;k>=1; k/=2) {
                heightmap.buffer[ i + j*scale ] += perlinNoise2D(i, j, k, P) * coef;
                coef /= 2.0;
            }
            if (heightmap.min > heightmap.buffer[ i + j*scale]) {
                heightmap.min = heightmap.buffer[ i + j*scale];
            }
            if (heightmap.max < heightmap.buffer[ i + j*scale]) {
                heightmap.max = heightmap.buffer[ i + j*scale];
            }
        }
    }
    
    return heightmap;
}
