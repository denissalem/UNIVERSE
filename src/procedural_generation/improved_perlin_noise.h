#ifndef UNIVERSE_IMPROVED_PERLIN_NOISE_H
#define UNIVERSE_IMPROVED_PERLIN_NOISE_H

#include "heightmap.h"

// Interpolation formula : 6t^5 - 15t^4 + 10t^3
#define interpolation(t) (6 * t * t * t * t * t - 15 * t * t * t * t + 10 * t * t * t)

Heightmap   getHeightmapFromPerlinNoise2D(unsigned int scale);

#endif
