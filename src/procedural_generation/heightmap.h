#ifndef UNIVERSE_HEIGHTMAP_H
#define UNIVERSE_HEIGHTMAP_H

typedef struct _Heightmap {
    double * buffer;
    float min;
    float max;
} Heightmap;

#endif
